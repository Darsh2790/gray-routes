package com.dcdroid.grayroutesdarshitassignment.model;

import android.graphics.Bitmap;

public class InfoWindowData {
    private Bitmap image;
    private String address;

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}