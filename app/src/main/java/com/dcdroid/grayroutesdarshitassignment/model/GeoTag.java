package com.dcdroid.grayroutesdarshitassignment.model;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

public class GeoTag {


    private LatLng location;
    private Bitmap image;


    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
