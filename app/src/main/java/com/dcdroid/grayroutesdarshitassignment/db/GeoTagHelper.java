package com.dcdroid.grayroutesdarshitassignment.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

import com.dcdroid.grayroutesdarshitassignment.model.GeoTag;
import com.dcdroid.grayroutesdarshitassignment.utils.DbBitmapUtility;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class GeoTagHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "GeoTags.db";
    public static final String GEOTAG_TABLE_NAME = "GeoTags";
    public static final String GEOTAG_COLUMN_ID = "id";
    public static final String GEOTAG_COLUMN_LATITUDE = "lat";
    public static final String GEOTAG_COLUMN_LONGITUDE = "lng";
    public static final String GEOTAG_COLUMN_IMAGE = "image";


    public GeoTagHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table  "+ GEOTAG_TABLE_NAME +
                        "("+GEOTAG_COLUMN_ID+" integer primary key, "+GEOTAG_COLUMN_LATITUDE+" text,"+ GEOTAG_COLUMN_LONGITUDE +" text,"+ GEOTAG_COLUMN_IMAGE +" blob)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+GEOTAG_TABLE_NAME);
        onCreate(db);
    }


    public boolean insertGeoTag (String lat, String lng,byte[] image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GEOTAG_COLUMN_LATITUDE, lat);
        contentValues.put(GEOTAG_COLUMN_LONGITUDE, lng);
        contentValues.put(GEOTAG_COLUMN_IMAGE, image);
        db.insert(GEOTAG_TABLE_NAME, null, contentValues);
        return true;
    }

    public ArrayList<GeoTag> getAllGeoTags() {
        ArrayList<GeoTag> array_list = new ArrayList<GeoTag>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+GEOTAG_TABLE_NAME, null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            GeoTag geoTag = new GeoTag();
            double lat = Double.parseDouble(res.getString(res.getColumnIndex(GEOTAG_COLUMN_LATITUDE)));
            double lng = Double.parseDouble(res.getString(res.getColumnIndex(GEOTAG_COLUMN_LONGITUDE)));
            LatLng geoLocation = new LatLng(lat,lng);
            geoTag.setLocation(geoLocation);
            Bitmap image=null;
            byte[] bytes_images = res.getBlob(res.getColumnIndex(GEOTAG_COLUMN_IMAGE));
            if(bytes_images!=null)
            {
                 image = DbBitmapUtility.getImage(bytes_images);
            }
            geoTag.setImage(image);
            array_list.add(geoTag);
            res.moveToNext();
        }
        return array_list;
    }
}
