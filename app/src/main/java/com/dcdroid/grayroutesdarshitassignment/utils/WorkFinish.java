package com.dcdroid.grayroutesdarshitassignment.utils;

public interface WorkFinish {
    void onWorkFinish(Boolean check);
}