package com.dcdroid.grayroutesdarshitassignment.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dcdroid.grayroutesdarshitassignment.R;
import com.dcdroid.grayroutesdarshitassignment.model.InfoWindowData;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public CustomInfoWindowGoogleMap(Context ctx) {
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.custom_info_window, null);

        TextView name_tv = view.findViewById(R.id.name);
        TextView details_tv = view.findViewById(R.id.details);
        ImageView img = view.findViewById(R.id.pic);




        name_tv.setText("Tap here for details");
        details_tv.setText("Address : "+marker.getTitle());

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();
        Bitmap bitmap = infoWindowData.getImage();

        if(bitmap!=null)
        {

            img.getLayoutParams().width = bitmap.getWidth();
            img.getLayoutParams().height = bitmap.getHeight();
            img.setImageBitmap(bitmap);
            img.requestLayout();
        }




        return view;
    }
}
 