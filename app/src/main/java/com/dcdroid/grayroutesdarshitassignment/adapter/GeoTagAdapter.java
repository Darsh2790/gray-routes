package com.dcdroid.grayroutesdarshitassignment.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.dcdroid.grayroutesdarshitassignment.MapActivity;
import com.dcdroid.grayroutesdarshitassignment.R;
import com.dcdroid.grayroutesdarshitassignment.model.GeoTag;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;


public class GeoTagAdapter extends
        RecyclerView.Adapter<GeoTagAdapter.MyViewHolder> {

    private ArrayList<GeoTag> geoTags;

    private Context context;
    private Activity activity;


    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvAddress;
        public TextView tvLatitude;
        public TextView tvLongitude;
        public ImageView ivGeoImage;
        public LinearLayout mainLayout;

        public MyViewHolder(View view) {
            super(view);
            tvAddress = (TextView) view.findViewById(R.id.tv_address);
            tvLatitude = (TextView) view.findViewById(R.id.tv_latitude);
            tvLongitude = (TextView) view.findViewById(R.id.tv_longitude);
            ivGeoImage = (ImageView) view.findViewById(R.id.image);
            mainLayout = (LinearLayout) view.findViewById(R.id.main_layout);
        }
    }

    public GeoTagAdapter(Activity activity, Context context, ArrayList<GeoTag> geoTags) {
        this.geoTags = geoTags;
        this.context=context;
        this.activity=activity;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final GeoTag geoTag = geoTags.get(position);
        holder.tvAddress.setText("Address : "+getAddress(geoTag.getLocation().latitude,geoTag.getLocation().longitude));
        holder.tvLatitude.setText("Latitude : "+ geoTag.getLocation().latitude+"");
        holder.tvLongitude.setText("Longitude : "+ geoTag.getLocation().longitude+"");

        if(geoTag.getImage()!=null)
        holder.ivGeoImage.setImageBitmap(geoTag.getImage());


        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent data = new Intent();
                String location = geoTag.getLocation().latitude+","+geoTag.getLocation().longitude;
                data.setData(Uri.parse(location));
                activity.setResult(RESULT_OK, data);
                activity.finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return geoTags.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_geo_tag_detail, parent, false);
        return new MyViewHolder(v);
    }
    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            Log.v("IGA", "Address" + add);

            return  add;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            /*  Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();*/
            return "Unknown Location";
        }
    }
}
