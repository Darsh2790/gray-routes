package com.dcdroid.grayroutesdarshitassignment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.dcdroid.grayroutesdarshitassignment.db.GeoTagHelper;
import com.dcdroid.grayroutesdarshitassignment.model.GeoTag;
import com.dcdroid.grayroutesdarshitassignment.model.InfoWindowData;
import com.dcdroid.grayroutesdarshitassignment.utils.CustomInfoWindowGoogleMap;
import com.dcdroid.grayroutesdarshitassignment.utils.DbBitmapUtility;
import com.dcdroid.grayroutesdarshitassignment.utils.PermUtil;
import com.dcdroid.grayroutesdarshitassignment.utils.WorkFinish;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private static final int CAMERA_REQUEST = 1888;
    private static final int SELECTED_GEOTAG_REQUEST = 101;

    private LatLng tappedLocation;

    GeoTagHelper geoTagHelper;
    ArrayList<Marker> markers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);


        findViews();
        askPermission();


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            MapActivity.this, R.raw.map_style));
            if (!success) {
                Log.e("darshit", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("----", "Can't find style. Error: ", e);
        }


        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                tappedLocation = latLng;
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intent, CAMERA_REQUEST);


            }
        });


        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {


                Intent intent = new Intent(MapActivity.this, ListActivity.class);
                intent.putExtra("latitude", marker.getPosition().latitude);
                intent.putExtra("longitude", marker.getPosition().longitude);
                startActivityForResult(intent, SELECTED_GEOTAG_REQUEST);
            }
        });


        if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);


        fetchSavedGeoTags();


    }

    private void findViews() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        geoTagHelper = new GeoTagHelper(MapActivity.this);
    }


    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(MapActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            Log.v("IGA", "Address" + add);

            return add;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            /*  Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();*/
            return "Unknown Location";
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST) {

            byte[] image_geo_tag = null;
            Bitmap photo = null;
            /*Store into db*/
            if (data != null) {
                photo = (Bitmap) data.getExtras().get("data");
                image_geo_tag = DbBitmapUtility.getBytes(photo);
            }

            if (geoTagHelper.insertGeoTag(tappedLocation.latitude + "", tappedLocation.longitude + "", image_geo_tag)) {
                Log.e("Location:", "saved");
            } else {
                Log.e("Location:", "not saved");
            }
            ////////////////

            String address = getAddress(tappedLocation.latitude, tappedLocation.longitude);

            MarkerOptions markerOptions = new MarkerOptions().position(tappedLocation).title(address).icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.pin));


            InfoWindowData info = new InfoWindowData();
            info.setImage(photo);
            info.setAddress(address);

            CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(this);
            mMap.setInfoWindowAdapter(customInfoWindow);

            Marker marker = mMap.addMarker(markerOptions);
            marker.setTag(info);
            marker.showInfoWindow();


        } else if (requestCode == SELECTED_GEOTAG_REQUEST) {
            if (resultCode == RESULT_OK) {
                //CENTER MAP to selected GEOTAG
                String returnedResultLocation = data.getData().toString();
                double lat = Double.parseDouble(returnedResultLocation.split(",")[0]);
                double lng = Double.parseDouble(returnedResultLocation.split(",")[1]);
                LatLng centerPoint = new LatLng(lat, lng);

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(centerPoint);
                LatLngBounds bounds = builder.build();
                int padding = 0; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                mMap.animateCamera(cu);

            }
        }
    }

    public void askPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermUtil.checkForRequiredPermissions(MapActivity.this, new WorkFinish() {
                @Override
                public void onWorkFinish(Boolean check) {

                    if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    if (mMap != null) {
                        mMap.setMyLocationEnabled(true);
                    }
                }
            });
        }

    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public void fetchSavedGeoTags() {
        if (mMap != null) {
            mMap.clear();
        }

        ArrayList<GeoTag> savedGeoTags = new ArrayList<>();
        savedGeoTags = geoTagHelper.getAllGeoTags();
        markers = new ArrayList<>();

        for (int geoTag = 0; geoTag < savedGeoTags.size(); geoTag++) {
            if (mMap != null) {


                String address = getAddress(savedGeoTags.get(geoTag).getLocation().latitude, savedGeoTags.get(geoTag).getLocation().longitude);

                MarkerOptions markerOptions = new MarkerOptions().position(savedGeoTags.get(geoTag).getLocation()).title(getAddress(savedGeoTags.get(geoTag).getLocation().latitude, savedGeoTags.get(geoTag).getLocation().longitude)).icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.pin));
                InfoWindowData info = new InfoWindowData();
                info.setImage((savedGeoTags.get(geoTag).getImage()));
                info.setAddress(address);

                CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(this);
                mMap.setInfoWindowAdapter(customInfoWindow);

                Marker marker = mMap.addMarker(markerOptions);
                marker.setTag(info);
                markers.add(marker);
            }
        }

        if(mMap!=null)
        {
            final Handler handler = new Handler();

            final Runnable r = new Runnable() {
                public void run() {

                    try{
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (Marker marker : markers) {
                            builder.include(marker.getPosition());
                        }
                        LatLngBounds bounds = builder.build();
                        int padding = 50; // offset from edges of the map in pixels
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        mMap.animateCamera(cu);
                    }catch (Exception e)
                    {

                    }



                }
            };
            handler.postDelayed(r, 1000);
        }

    }
}
