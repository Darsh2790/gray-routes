package com.dcdroid.grayroutesdarshitassignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.dcdroid.grayroutesdarshitassignment.adapter.GeoTagAdapter;
import com.dcdroid.grayroutesdarshitassignment.db.GeoTagHelper;
import com.dcdroid.grayroutesdarshitassignment.model.GeoTag;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {


    GeoTagHelper geoTagHelper;
    GeoTagAdapter geoTagAdapter;
    RecyclerView recyclerViewGeoTag;
    ArrayList<GeoTag> geoTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        findViews();

        Intent intent = getIntent();
        double latitude = intent.getDoubleExtra("latitude", -1);
        double longitude = intent.getDoubleExtra("longitude", -1);
        loadGeoTags(latitude, longitude);


    }

    private void findViews() {
        geoTagHelper = new GeoTagHelper(ListActivity.this);

        recyclerViewGeoTag = (RecyclerView) findViewById(R.id.recyclerViewGeoTag);
    }

    private void loadGeoTags(double lat, double lng) {

        geoTags = geoTagHelper.getAllGeoTags();
        int index = 0;
        if (lat != -1 && lng != -1) {
            /*move the selected geoTag to top*/

            for (int i = 0; i < geoTags.size(); i++) {
                if (lat == geoTags.get(i).getLocation().latitude && lng == geoTags.get(i).getLocation().longitude) {
                    index = i;
                    break;
                }
            }
        }
        if (index != 0) {
            GeoTag topGeoTag = geoTags.get(index);
            geoTags.remove(index);
            geoTags.add(0, topGeoTag);
        }


        geoTagAdapter = new GeoTagAdapter(ListActivity.this,ListActivity.this, geoTags);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewGeoTag.setLayoutManager(layoutManager);
        recyclerViewGeoTag.setAdapter(geoTagAdapter);
    }
}
