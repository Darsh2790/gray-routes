Dear Darshit,

Pl find below problem statement of an assignment for Android Development, deadline to submit/share the solution is tomorrow Friday 21st June 2019 by 8 pm
Geo- tagging app:

The app will have two screens (can be activities or fragments) :
One for displaying a Map and other to display a list;

Map screen:

This should be an interactive screen. It should allow the user to choose any point on the map and retrieve the latitude and longitude using action like double-tap or long press or any similar action. After selecting, the app should make the user click a photo. The latitude and longitude along with the photo should be stored appropriately.
Each tagged point should be displayed on the map with a suitable marker.

List screen:

This screen contains a list of items. Each item should contain an image and a text.
Image should be the image taken while tagging on the map. The text should contain the lat-long pair and the address. On click of the item, the app should navigate to the Map screen with the tagged point in the center.

On clicking the tagged point marker, the app should navigate to the list screen showing the particular list-item related to the clicked location to the top.

We urge you to use best practices while coding and create an intuitive UI.

Extra points if you can write test scripts.

Note: You are required to share the code using any preferred and easily accessible VCS or code sharing platform or send the project in a zipped file.